#!/bin/bash

set -e

CONFIGDIR=/config
CONFIGFILE=$CONFIGDIR/plexpy.ini
DATADIR=/data

if [ "$1" = 'plexpy' ]; then
    shift

    chown -R plexpy:plexpy $CONFIGDIR
    chown -R plexpy:plexpy $DATADIR

    exec python /opt/plexpy/PlexPy.py \
        -v \
        --port 8080 \
        --datadir $DATADIR \
        --config $CONFIGDIR/plexpy.ini \
        --nolaunch
fi

exec "$@"
