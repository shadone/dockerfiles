#!/bin/bash

set -e

CONFIGDIR=/config
CONFIGFILE=$CONFIGDIR/collectd.conf

if [ "$1" = 'collectd' ]; then
    shift

    exec collectd -f -C $CONFIGFILE $*
fi

exec "$@"
