collectd
========

# Building

```
$ docker build -t ddenis/collectd .
```

# Running

```
$ docker run -d --restart=always \
		--volume /opt/collectd/config:/config \
		--name collectd \
		ddenis/collectd
```
