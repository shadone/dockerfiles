# docker-crashplan

## Description

A Dockerfile for [CrashPlan](http://www.code42.com/crashplan/).

## Volumes

### `/data`

`/data` contains CrashPlan configuration data.

### `/backups`

`/backups` contains local backups.

## Ports

### 4242

Backup service.

### 4243

UI service.

## Environment Variables

### `MAX_MEMORY_MB`

The maximum memory limit in megabytes. Defaults to `1024`. Code42 [recommends](http://support.code42.com/CrashPlan/Latest/Troubleshooting/CrashPlan_Runs_Out_Of_Memory_And_Crashes) allocating 1 GB per 1 TB of storage.


## Running

Create image: `sudo docker build -t ddenis/crashplan:3.7.0 .`

Make it latest: `sudo docker tag -f ddenis/crashplan:3.7.0 ddenis/crashplan:latest`

Run service: `sudo docker run --restart=always -d --volume /opt/crashplan/data:/data --volume /opt/crashplan/backups:/backups --expose 4242 -p 127.0.0.1:42430:4243 --name crashplan ddenis/crashplan`

Edit configuration file `/opt/crashplan/data/conf/custom_sample.properties`

Connect remotely with a gui:

```
# port forward
$ ssh -L 42430:localhost:42430 your-host-running-crashplan
# edit local crashplan gui config file
#   OS X: ~/Library/Application\ Support/CrashPlan/ui.properties
# add the following line:
#   servicePort=42430
```