#!/bin/bash

set -e

CONFIGDIR=/config
CRONDIR=/config/cron.d
DATADIR=/data

if [ "$1" = 'gmvault' ]; then
    shift

    test -d $CONFIGDIR/dotgmvault || rm -f $CONFIGDIR/dotgmvault
    test -e $CONFIGDIR/dotgmvault || mkdir $CONFIGDIR/dotgmvault
    ln -sf $CONFIGDIR/dotgmvault $HOME/.gmvault

    rm -rf $HOME/gmvault-db; ln -sf $DATADIR $HOME/gmvault-db

    if [ -d $CRONDIR ]; then
        (cd $CRONDIR
        for i in *; do
            chown root:root $CRONDIR/$i
            chmod 644 $CRONDIR/$i
            ln -sf $CRONDIR/$i /etc/cron.d/$i
        done)
    fi

    exec /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
fi

exec "$@"
