gmvault
=======

# Building

```
$ sudo docker build --tag ddenis/gmvault:latest .
```

# Running

```
$ sudo docker run -d --restart=always \
		--volume /opt/gmvault/config:/config \
		--volume /opt/gmvault/data:/data \
		--name gmvault \
		ddenis/gmvault
```

