#!/bin/sh

git submodule update plexpy-upstream
sha1=$(cd plexpy-upstream && git rev-parse HEAD)

docker build --tag ddenis/plexpy:$sha1 .
