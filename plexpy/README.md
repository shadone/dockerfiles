plexpy
======

# Building

```
$ ./build.sh
```

# Running

```
$ docker run -d --restart=always \
		--volume /opt/plexpy/config:/config \
		--volume /opt/plexpy/data:/data \
		-p 127.0.0.1:8080:8080 \
		--name plexpy \
		ddenis/plexpy
```
