#!/bin/bash

set -e

CONFIGDIR=/config

if [ "$1" = 'plex' ]; then
    shift

    if [ "$PLEX_UID" != 'unspecified' ]; then
	    usermod --uid $PLEX_UID plex
	fi

    chown plex:plex $CONFIGDIR

    exec /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
fi

exec "$@"
