plexmediaserver
===============

# Building

```
$ make docker
$ make release
```

# Running

Run plexmediaserver container:

```
$ sudo docker run -d --restart=always \
		--volume /opt/plex/config:/config \
		--volume /opt/plex/data:/data:ro \
		-p 32400 \
		--name plexmediaserver \
		ddenis/plexmediaserver
```

After running it for the first time it creates configuration files in /config, you will need to edit it to allow network access. Open /config/Plex Media Server/Preferences.xml and add attribute to Preferences tag `allowedNetworks="10.0.0.0/255.0.0.0"`, so it looks like this:

```
<?xml version="1.0" encoding="utf-8"?>
<Preferences allowedNetworks="10.0.0.0/255.0.0.0" MachineIdentifier="41502e60-5dee-4da9-a3f7-5eec5d7c6da8" ProcessedMachineIdentifier="f88c801494e1f55560daa60be2fcd36943a85d36" />
```
